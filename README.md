# mycode (FirstProject-062022)

The purpose of this project is to learn how to submit projects to gitlab. Additonally wanting to learn how to version control projects with git

## Getting Started

These instructions will get you a copy of the project up and running on your local machine
for development and testing purposes. See deployment for notes on how to deploy the project
on a live system.

### Prerequisites

What things are needed to install the software and how to install them. For now, maybe copy in:
"How to Install the Language: "

## Built With

* [Ansible](https://www.ansible.com)
* [Python](https://www.python.org/)

## Authors

* Jacob Blank - FirstProject - [YourWebsite](https://example.com/)
